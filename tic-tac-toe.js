function draw_field(n=3) {
    var grid = $("#game-grid");
    board_size = n;
    grid.height(grid.width());
    cell_size = $(".grid-item")[0].offsetWidth;
    grid.css("grid-template-rows", (cell_size + "px ").repeat(n));
    grid.css("grid-template-columns", (cell_size + "px ").repeat(n));
    var a = [];
    board = [];
    for (var i = 0; i < n; i++)
        a.push(null);
    for (var i = 0; i < n; i++)
        board.push(a.slice());
    $("#cross-score").text(cross_score);
    $("#circle-score").text(circle_score);
}

function make_turn(cell_num) {
    var x = cell_num % board_size;
    var y = Math.floor(cell_num / board_size);
    if (board[y][x] !== null)
        return;
    draw_figure(x, y);
    change_turn();
    turn_num += 1;
    winner = check_board();
    console.log(winner == null);
    if (winner != null || turn_num == board_size * board_size)
        claim_winner(winner);
    else
        return;

}

function draw_figure(x, y) {
    board[y][x] = turn;
    var cell = $($(".grid-item").get(y * board_size + x));
    cell.html("<img src=\"./static/" + turn + ".svg\"" + "width=\"" + (cell_size - 20) + "px\" height=\"" + (cell_size - 20) + "px\">");
}

function change_turn() {
    $("#" + turn + "_turn").hide()
    turn = not[turn];
    $("#" + turn + "_turn").show()
}

function check_board() {
    var winner = null;
    for (var i = 0; i < board_size; i++) {
        var circles = 0;
        var crosses = 0;
        for (var j = 0; j < board_size; j++) {
            switch (board[i][j]) {
                case "cross":
                    crosses += 1;
                    break;
                case "circle":
                    circles += 1;
                    break;
            }
        }
        if (circles == board_size)
            winner = "Circles";
        else if (crosses == board_size)
            winner = "Crosses";
    }
    for (var i = 0; i < board_size; i++) {
        var circles = 0;
        var crosses = 0;
        for (var j = 0; j < board_size; j++) {
            switch (board[j][i]) {
                case "cross":
                    crosses += 1;
                    break;
                case "circle":
                    circles += 1;
                    break;
            }
        }
        if (circles == board_size)
            winner = "Circles";
        else if (crosses == board_size)
            winner = "Crosses";
    }

    var circles = 0;
    var crosses = 0;
    for (var i = 0; i < board_size; i++) {
        switch (board[i][i]) {
            case "cross":
                crosses += 1;
                break;
            case "circle":
                circles += 1;
                break;
        }
    }
    if (circles == board_size)
        winner = "Circles";
    else if (crosses == board_size)
        winner = "Crosses";

    var circles = 0;
    var crosses = 0;
    for (var i = 0; i < board_size; i++) {
        switch (board[i][board_size - i - 1]) {
            case "cross":
                crosses += 1;
                break;
            case "circle":
                circles += 1;
                break;
        }
    }
    if (circles == board_size)
        winner = "Circles";
    else if (crosses == board_size)
        winner = "Crosses";
    return winner;
}

function claim_winner(winner) {
    switch (winner) {
        case "Crosses":
            cross_score += 1;
            break;
        case "Circles":
            circle_score += 1;
            break;
    }
    $("#result").html(winner === null ? "Draw!" : winner + " won!");
    $("#blurer").show();
    $("#win-alert").show();
}

function restart() {
    $(".grid-item").html("");
    $("#result").html("");
    draw_field(board_size);
    $("#win-alert").hide();
    $("#blurer").hide();
    if (turn == "circle")
        change_turn();
    turn_num = 0;
}

var cell_size = null;
var turn = "cross";
var board = [];
var board_size = null;
var cross_score = 0;
var circle_score = 0;
var turn_num = 0;
not = {cross: "circle", circle: "cross"}
